﻿using System;
using Newtonsoft.Json;

namespace Brax.Web.DTO.WP
{
    public class WPTermLink:WPEmbeddableLink
    {
        [JsonProperty("taxonomy")]
        public string Taxonomy { get; set; }

        public WPTermLink()
        {
        }
    }
}

