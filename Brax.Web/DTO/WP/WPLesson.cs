﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Brax.Web.DTO.WP
{
    public class WPLesson
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("date_gmt")]
        public DateTime DateGmt { get; set; }


        [JsonProperty("modified")]
        public DateTime Modified { get; set; }

        [JsonProperty("modified_gmt")]
        public DateTime ModifiedGmt { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("link")]
        public string Link { get; set; }

        [JsonProperty("title")]
        public WPRenderedString Title { get; set; }

        [JsonProperty("content")]
        public WPRenderedString Content { get; set; }

        [JsonProperty("author")]
        public int Author { get; set; }

        [JsonProperty("featured_media")]
        public int FeaturedMedia { get; set; }

        [JsonProperty("menu_order")]
        public int MenuOrder { get; set; }

        [JsonProperty("comment_status")]
        public string CommentStatus { get; set; }

        [JsonProperty("ping_status")]
        public string PingStatus { get; set; }

        [JsonProperty("tags")]
        public List<int> Tags { get; set; }

        [JsonProperty("CFS")]
        public WPLessonCFS CFS { get; set; }

        [JsonProperty("_links")]
        public WPLessonLinks Links { get; set; }

        public WPLesson()
        {
        }
    }
}

