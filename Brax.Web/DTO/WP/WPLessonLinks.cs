﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Brax.Web.DTO.WP
{
    public class WPLessonLinks
    {
        [JsonProperty("self")]
        public List<WPSimpleLink> Self { get; set; }

        [JsonProperty("collection")]
        public List<WPSimpleLink> Collection { get; set; }

        [JsonProperty("about")]
        public List<WPSimpleLink> About { get; set; }

        [JsonProperty("author")]
        public IList<WPEmbeddableLink> Author { get; set; }

        [JsonProperty("replies")]
        public IList<WPEmbeddableLink> Replies { get; set; }

        [JsonProperty("wp:featuredmedia")]
        public IList<WPEmbeddableLink> WpFeaturedmedia { get; set; }

        [JsonProperty("wp:attachment")]
        public IList<WPSimpleLink> WpAttachment { get; set; }

        [JsonProperty("wp:term")]
        public IList<WPTermLink> WpTerm { get; set; }

        [JsonProperty("curies")]
        public IList<WPCuryLink> Curies { get; set; }

        public WPLessonLinks()
        {
        }
    }
}

