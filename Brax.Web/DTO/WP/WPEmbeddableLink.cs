﻿using System;
using Newtonsoft.Json;

namespace Brax.Web.DTO.WP
{
    public class WPEmbeddableLink:WPSimpleLink
    {
        [JsonProperty("embeddable")]
        public bool Embeddable { get; set; }

        public WPEmbeddableLink()
        {
        }
    }
}

