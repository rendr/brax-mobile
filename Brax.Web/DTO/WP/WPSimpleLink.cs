﻿using System;
using Newtonsoft.Json;

namespace Brax.Web.DTO.WP
{
    public class WPSimpleLink
    {
        [JsonProperty("href")]
        public string Href { get; set; }

        public WPSimpleLink()
        {
        }
    }
}

