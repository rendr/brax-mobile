﻿using System;
using Newtonsoft.Json;

namespace Brax.Web.DTO.WP
{
    public class WPCuryLink:WPSimpleLink
    {
        [JsonProperty("name")]
        public string Name { get; set; }



        [JsonProperty("templated")]
        public bool Templated { get; set; }

        public WPCuryLink()
        {
        }
    }
}

