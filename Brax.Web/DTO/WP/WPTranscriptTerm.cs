﻿using System;
using Newtonsoft.Json;

namespace Brax.Web.DTO.WP
{
    public class WPTranscriptTerm
    {
        [JsonProperty("transcript_timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("transcript_text")]
        public string Text { get; set; }

        public WPTranscriptTerm()
        {
        }
    }
}

