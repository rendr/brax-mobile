﻿using System;

namespace Brax.Web.DTO.WP
{
    public class WPRenderedString
    {
        public string Rendered
        {
            get;
            set;
        }

        public WPRenderedString()
        {
        }
    }
}

