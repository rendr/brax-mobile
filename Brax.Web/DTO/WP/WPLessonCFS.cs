﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Brax.Web.DTO.WP
{
    public class WPLessonCFS
    {
        [JsonProperty("is_pro")]
        public string IsPro { get; set; }

        [JsonProperty("lesson_excerpt")]
        public string LessonExcerpt { get; set; }

        [JsonProperty("lesson_duration")]
        public string LessonDuration { get; set; }

        [JsonProperty("wistia_key")]
        public string WistiaKey { get; set; }

        [JsonProperty("lesson_short_title")]
        public string LessonShortTitle { get; set; }

        [JsonProperty("transcript_terms")]
        public List<WPTranscriptTerm> TranscriptTerms { get; set; }

        [JsonProperty("additional_info")]
        public List<WPAdditionalInfo> AdditionalInfo { get; set; }

        public WPLessonCFS()
        {
        }
    }
}

