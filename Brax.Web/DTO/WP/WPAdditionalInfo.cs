﻿using System;
using Newtonsoft.Json;

namespace Brax.Web.DTO.WP
{
    public class WPAdditionalInfo
    {
        [JsonProperty("additional_info_item_title")]
        public string Title { get; set; }

        [JsonProperty("additional_info_item_description")]
        public string Description { get; set; }

        public WPAdditionalInfo()
        {
        }
    }
}

