﻿using System;
using Brax.Models;
using Brax.Web.DTO.WP;
using System.Collections.Generic;
using System.Linq;

namespace Brax.Web.Extensions
{
    public static class DtoToModelExtensions
    {
        //Lesson
        public static List<Lesson> ToModels(this List<WPLesson> dtos)
        {
            return dtos.Select(d => d.ToModel()).ToList();
        }

        public static Lesson ToModel(this WPLesson dto)
        {
            return new Lesson()
            { 
                Id = dto.Id,
                Title = dto.Title.Rendered, 
                ShortTitle = dto.CFS.LessonShortTitle,
                IsPro = dto.CFS.IsPro != "0",
                WistiaKey = dto.CFS.WistiaKey,
                Excerpt = dto.CFS.LessonExcerpt,
                Duration = TimeSpan.Parse("00:" + dto.CFS.LessonDuration),
                Transcript = dto.CFS.TranscriptTerms != null ? dto.CFS.TranscriptTerms.ToModels() : new List<TranscriptEntry>(),
                AdditionalInfo = dto.CFS.AdditionalInfo != null ? dto.CFS.AdditionalInfo.ToModels() : new List<TextBlurb>()

            };
        }

        //TranscriptEntry
        public static List<TranscriptEntry> ToModels(this List<WPTranscriptTerm> dtos)
        {
            return dtos.Select(d => d.ToModel()).ToList();
        }

        public static TranscriptEntry ToModel(this WPTranscriptTerm dto)
        {
            return new TranscriptEntry()
            {
                Timestamp = TimeSpan.Parse("00:" + dto.Timestamp),
                Text = dto.Text
            };
        }

        //AdditionalInfo
        public static List<TextBlurb> ToModels(this List<WPAdditionalInfo> dtos)
        {
            return dtos.Select(d => d.ToModel()).ToList();
        }

        public static TextBlurb ToModel(this WPAdditionalInfo dto)
        {
            return new TextBlurb()
            {
                Title = dto.Title,
                Description = dto.Description
            };
        }
    }
}

