﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Net;
using Newtonsoft.Json;
using Brax.Web.DTO.WP;
using Brax.Web.Extensions;
using Brax.Models;

namespace Brax.Web.Controllers
{
    public class APIController : Controller
    {
        protected WpV1Client Client = new WpV1Client ();

        public ActionResult Lesson ()
        {
            var list = Client.GetLessons ();
            return Json (list, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Playlist ()
        {

            var wc = new WebClient ();

            var wpJson = wc.DownloadString ("lesson");

            var wpLessons = JsonConvert.DeserializeObject<List<WPLesson>> (wpJson);
            return Json (wpLessons, JsonRequestBehavior.AllowGet);
        }


    }

    public class WpV1Client : WebClient
    {
        public WpV1Client ()
        {
            this.BaseAddress = "https://mobileup.io/wp-json/wp/v2/";
        }

        public List<Lesson> GetLessons ()
        {

            var jsonStr = DownloadString ("lesson");
            return JsonConvert.DeserializeObject<List<WPLesson>> (jsonStr).ToModels ();

        }

        //        public List<Playli
    }
}
