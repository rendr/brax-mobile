﻿using System;

namespace Brax.Models
{
    public class TranscriptEntry
    {
        public TimeSpan Timestamp
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public TranscriptEntry()
        {
        }
    }
}

