﻿using System;

namespace Brax.Models
{
    public class TextBlurb
    {
        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public TextBlurb()
        {
        }
    }
}

