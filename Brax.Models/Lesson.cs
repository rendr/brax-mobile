﻿using System;
using System.Collections.Generic;

namespace Brax.Models
{
    public class Lesson
    {
        public int Id
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string ShortTitle
        {
            get;
            set;
        }

        public bool IsPro
        {
            get;
            set;
        }

        public string Excerpt
        {
            get;
            set;
        }

        public TimeSpan Duration
        {
            get;
            set;
        }

        public string WistiaKey
        {
            get;
            set;
        }

        public List<TranscriptEntry> Transcript
        {
            get;
            set;
        }

        public List<TextBlurb> AdditionalInfo
        {
            get;
            set;
        }

        public Lesson()
        {
        }
    }
}

